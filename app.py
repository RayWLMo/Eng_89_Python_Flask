# Creating a file called app.py
# Using Python Flask to interact with a browser

# Installing and importing flask
from flask import Flask, redirect, url_for, render_template

# Creating an object of this class
app = Flask(__name__)  # Creating an app instance


# Creating a function to link to the home/default page
# Connecting the function to a browser
@app.route("/")  # Decorating the function with @app.route to set route in the browser
def index():
    return 'Engineering DevOps Team Website'


# Command to run: flask run


# Creating a welcome page
@app.route('/welcome/')  # Good practice to have as it loads page with/without slash
def welcome():
    return render_template('welcome.html')


# Create a decorator to route traffic to login page and displaying 2 messages in form of h1 and h2


@app.route('/login/')
def login():  # `redirect` and url_for used for redirecting traffic to a page (needs importing)
    return'<h1>Welcome to the login page<h1>' \
          '<h2>Please enter your login details<h2>'


if __name__ == '__main__':
    app.run(debug=True)

# Adding the HTML file to redirect from Python flask to .html file
# Need to create a folder - `templates`
# project folder
    # templates folder
        # welcome.html
    # app.py

# Handling the website when the page is unavailable
# Redirecting the user if they visit login page


@app.route('/page-error')
def page_error():
    return redirect(url_for('welcome'))  # This will redirect the user to the welcome page


@app.route('/<username>/')  # passing variable provided by the user in the browser
def greet_user(username):
    return f'Welcome to the website {username}'
