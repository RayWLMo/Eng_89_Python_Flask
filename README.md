# Model View Controller (MVC) w/ Python Flask

## Python Flask
- Flask is a python micro-framework
- Flask is used to develop web apps

### Using Python Flask to interact with a browser
- Creating a file called app.py

#### Installing and importing flask
```python
from flask import Flask
```
#### Creating an object of this class
```python
app = Flask(__name__)  # Creating an app instance
```
#### Creating a function to link to the home/default page and connecting the function to a browser
```python
@app.route("/")  # Decorating the function with @app.route to set route in the browser
def index():
    return 'Engineering DevOps Team Website'
```
- Command to run flask: `flask run`

#### Creating a welcome page using headers
```python
@app.route('/welcome/')  # Good practice to have as it loads page with/without slash
def welcome():
    return '<h1>Welcome to the Engineering DevOps Team Website<h1>'
```
#### Create a decorator to route traffic to login page using 2 headings
```python
@app.route('/login')
def login():
    return'<h1>Welcome to the login page<h1>' \
          '<h2>Please enter your login details<h2>'
```
#### Redirecting the user to the login page
- This can be useful when handling the website when the page is unavailable
```python
@app.route('/page-error')
def page_error():
    return redirect(url_for('welcome'))  # This will redirect the user to the welcome page
```
#### Using user input in the browser to pass information
```python
@app.route('/<username>/')  # passing variable provided by the user in the browser
def greet_user(username):
    return f'Welcome to the website {username}'
```

## HTML Files
### Adding the HTML file to redirect from Python flask to .html file
- Need to create a folder - `templates`
- project folder
    - templates folder 
        - welcome.html
    - app.py